// ==UserScript==
// @name Common_Shura.uc.js
// @description 一些对Firefox进行小优化的脚本
// @author Shura
// @include main
// @charset utf-8
// @version 2014.02.10
// @downloadURL http://git.oschina.net/shura/UserChromeJS/raw/master/Common_Shura.uc.js
// @homepageURL http://git.oschina.net/shura/UserChromeJS/
// @reviewURL http://git.oschina.net/shura/UserChromeJS/issues
// ==/UserScript==

/*Firefox冷启动加速*/
location == "chrome://browser/content/browser.xul" && (function(){

let { classes: Cc, interfaces: Ci, utils: Cu, results: Cr } = Components;
Cu.import("resource://gre/modules/AddonManager.jsm");

//定义扩展ID
const ABP_ID = '{d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d}';
const LBE_ID = 'UIEnhancer@girishsharma';
const EHHAP_ID = 'elemhidehelper@adblockplus.org';

function disableAddon(Addon_ID,disable){
AddonManager.getAddonByID(Addon_ID,function(addon){
addon.userDisabled = disable;
});
}
//  启用扩展
disableAddon(ABP_ID,false);
disableAddon(LBE_ID,false);
disableAddon(EHHAP_ID,false);

// Firefox 关闭时禁用扩展
window.addEventListener("unload", function(){
disableAddon(ABP_ID,true);
disableAddon(LBE_ID,true);
disableAddon(EHHAP_ID,true);
}, false);
})()