// ==UserScript==
// @name           Firefox_Goagent_Shura.uc.js
// @namespace      Firefox_Goagent_Shura.uc.js
// @description    在Firefox中调用goagent程序的脚本
// @author         Shura
// @include        main
// @charset        UTF-8
// @version        0.1
// @homepageURL    http://chenxuefeng.net.cn/
// @downloadURL http://git.oschina.net/shura/UserChromeJS/raw/master/Firefox_Goagent_Shura.uc.js
// @reviewURL      http://git.oschina.net/shura/UserChromeJS/
// @note 		   在Firefox中调用goagent
// @include        chrome://browser/content/browser.xul
// ==UserScript==
var shura = {
exec: function(path, args) {
            args = args || [];
            var args_t=args.slice(0);
        for (var i=0; i<args_t.length; i++) {
          args_t[i] = args_t[i].replace(/%u/g, gBrowser.currentURI.spec);
        }

        var file = Cc['@mozilla.org/file/local;1'].createInstance(Ci.nsILocalFile);
        file.initWithPath(path);
        if (!file.exists()) {
          Cu.reportError('File Not Found: ' + path);
          return;
        }

        if (!file.isExecutable()) {
          file.launch();
        }
        else {
          var process = Cc['@mozilla.org/process/util;1'].createInstance(Ci.nsIProcess);
          process.init(file);
          process.run(false, args_t, args_t.length);
        }
      },

createBtn: function() {
    	var TARGET = null;
		var Btn = document.createElement("toolbarbutton");
		Btn.id = "shura-goagent";
		Btn.setAttribute("type", "none");
		Btn.setAttribute("class", "toolbarbutton-1 chromeclass-toolbar-additional");
		Btn.setAttribute("onclick", "shura.exec(this.path, this.args);");
		Btn.style.listStyleImage = "url(data:image/ico;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAQAQAAAAAAAAAAAAAAAAAAAAAAAD9/UP///8/////P////z////8/////P////z////8/////P////z////8/////P////z////8/////P//09FH///8//5//e/+a/3j/mv94/5r/eP+a/3j/mv94/5r/eP+a/3j/mv94/5r/eP+a/3j/mv94/5r/eP+f/3v/9PRR////P/+a/3j/wsKH/52dbf+KimD/g4Nb/39/WP95eVT/dXVR/3Z2Uv9+flf/mZlq/8PDh//p6aL/mv94////P////z//mv94/8PDh/+urnn/tbV+/8rKjP/W1pX/0tKS/76+hP+ammv/bW1M/1FROP+JiV//z8+Q/5r/eP///z////8//5r/eP/s7KT/5+eg/+3tpf/4+Kz///+x//39sP/n56D/urqB/3h4U/82NiX/bm5M/8LCh/+a/3j///8/////P/+a/3j///+x////sf///7H//f2w//b2q//s7KT/z8+Q/52dbf9eXkH/PDwq/3l5VP/IyIv/mv94////P////z//mv94////sf/8/K//8/Op/+Pjnv/FxYn/n59u/3p6Vf9bWz//S0s0/21tTP+oqHX/3t6a/5r/eP///z////8//5r/eP/19ar/2NiW/6+vef+BgVr/XFxA/1NTOv9cXED/c3NQ/5iYav/ExIj/4uKd//b2q/+a/3j///8/////P/+a/3j/xcWJ/4uLYP9aWj7/QEAs/1lZPv+EhFz/ra14/9LSkv/v76b/+vqu//7+sP///7H/mv94////P////z//mv94/4uLYP9UVDr/UVE4/3d3U/+8vIL/3d2Z//Dwp//7+67///+x////sf///7H///+x/5r/eP///z////8//5r/eP94eFP/TU01/2dnR/+pqXX/6emi//n5rf/+/rD///+x////sf/7+67//Pyv//7+sP+a/3j///8/////P/+a/3j/k5Nm/2NjRf9lZUb/jIxh/8jIi//X15X/2tqX/9nZl//W1pX/1NST/+Pjnv/09Kn/mv94////P////z//mv94/9LSkv+jo3H/e3tV/2hoSP+EhFz/jo5j/5GRZf+QkGT/j49j/6KicP/Hx4r/6uqi/5r/eP///z////8//5r/eP/6+q7/4+Oe/8PDh/+lpXP/mppr/5eXaf+VlWf/l5dp/5+fbv+8vIL/2tqX//LyqP+a/3j///8/////P/+x/4X/sf+F/5r/eP+a/3j/mv94/5r/eP+a/3j/mv94/5r/eP+a/3j/mv94/5r/eP+a/3j/n/97////P//9/UP///8/////P////z////8/////P////z////8/////P////z////8/////P////z////8/////P//8/EP/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==)";
		Btn.setAttribute("label","shura-goagent");
		//Btn.setAttribute("oncommand","shura.exec(this.path, this.args);");
		Btn.path = Components.classes['@mozilla.org/file/directory_service;1'].getService(Components.interfaces.nsIProperties).get("ProfD", Components.interfaces.nsILocalFile).path + '\\goa\\goagent.exe';
        Btn.args = '';
		Btn.setAttribute("tooltiptext","打开goagent");
		document.insertBefore(document.createProcessingInstruction('xml-stylesheet', 'type="text/css" href="data:text/css;utf-8,' + encodeURIComponent(
		'\
		#uac_statusbar_panel {\
		  -moz-appearance: none !important;\
		  border-style: none !important;\
		  border-radius: 0 !important;\
		  padding: 0 3px !important;\
		  margin: 0 !important;\
		  background: transparent !important;\
		  box-shadow: none !important;\
		  -moz-box-align: center !important;\
		  -moz-box-pack: center !important;\
		  min-width: 18px !important;\
		  min-height: 18px !important;\
		          }\
		#uac_statusbar_panel > .toolbarbutton-icon {\
			max-width: 18px !important;\
		    padding: 0 !important;\
		    margin: 0 !important;\
		}\
		#uac_statusbar_panel dropmarker{display: none !important;}\
		    ') + '"'), document.documentElement);
		Btn.style.padding = "0px 2px";
		
		var toolbar = document.getElementById("urlbar-icons");//图标显示位置，urlbar-icons  status-bar addon-bar searchbar TabsToolbar alltabs-button bookmarks-menu-button
		if (this.TARGET != null) {
			this.TARGET = document.getElementById(this.TARGET);
		}
		toolbar.insertBefore(Btn, this.TARGET);
    }
};
	shura.createBtn();