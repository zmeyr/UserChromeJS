// ==UserScript==
// @name           JSCSS_Highlight.uc.js
// @namespace      JSCSS_Highlight.uc.js
// @description    高亮 js、css代码
// @author         Griever
// @note           Shura 微改
// @license        MIT License
// @compatibility  Firefox 23
// @charset        UTF-8
// @include        main
// @version        0.0.6
// ==/UserScript==

(function(){

var BASE_Style = {
	MlutiComment   : 'color:#080;',
	LineComment    : 'color:#080;',
	DoubleQuotation: 'color:#c11;',
	SingleQuotation: 'color:#c11;',
	URL            : '',
	CDATA          : 'color:#c11;',
};

var JS_Style = {
	keyword  : 'color:#a09;',
	object   : 'color:#c15;',
	method   : 'color:#027;',
	property : 'color:#06a;',
	hougen   : 'color:#06a;',
	CDATA    : 'color:#c11;',
};

var CSS_Style = {
	keyword  : 'color:#a09;',
	pseudo   : 'color:#a09;',
	property : 'color:#06a;',
	hougen   : 'color:#06a;',
};

var JS = {};
var CSS = {};
var XML = {};
var BASE = {};


JS.keyword = [
"abstract","boolean","break","byte","case","catch","char","class","const",
"continue","debugger","default","delete","do","double","else","enum","export",
"extends","false","final","finally","float","for","function","goto","if",
"implements","import","in","instanceof","int","interface","long","native","new",
"null","package","private","protected","public","return","short","static",
"super","switch","synchronized","this","throw","throws","transient","true","try",
"typeof","var","void","volatile","while","with",
"let","yield","infinity","NaN","undefined","of"
];

JS.object = [
"Array","Boolean","Date","Error","EvalError","Function","Number","Object",
"RangeError","ReferenceError","RegExp","String","SyntaxError","TypeError",
"URIError","eval","decodeURI","decodeURIComponent","encodeURI",
"encodeURIComponent","escape","unescape","isFinite","isNaN","parseFloat",
"parseInt"
];

JS.method = [
"addEventListener","removeEventListener","handleEvent","alert","prompt",
"confirm","setTimeout","setInterval","clearTimeout","clearInterval","toString",
"toSource"
];

JS.property = [
"window","document","prototype","callee","caller","event","arguments"
];

JS.hougen = [
"$","jQuery", "opera","chrome", "gBrowser","Components",
"GM_log","GM_addStyle","GM_xmlhttpRequest","GM_openInTab",
"GM_registerMenuCommand","GM_unregisterMenuCommand","GM_enableMenuCommand",
"GM_disableMenuCommand","GM_getResourceText","GM_getResourceURL",
"GM_setValue","GM_getValue","GM_listValues","GM_deleteValue",
"GM_getMetadata","GM_setClipboard","GM_safeHTMLParser","GM_generateUUID"
];



CSS.keyword = [
"@import","@charset","@media","@font-face","@page","@namespace","@keyframes",
"!important",
"@-moz-document",
];

CSS.pseudo = [
":before",":after",":first-letter",":first-line",
"::before","::after","::first-letter","::first-line","::selection",
":root",":not", ":link",":visited",":active",":focus",":hover",
":target",":enabled",":disabled",":checked",":default",":empty",
":nth-child",":nth-of-type",":first-child",":last-child",":only-child",
":nth-last-child",":nth-last-of-type",
":first-of-type",":last-of-type",":only-of-type",

"::-moz-anonymous-block","::-moz-anonymous-positioned-block",":-moz-any",
":-moz-any-link",":-moz-bound-element",":-moz-broken","::-moz-canvas",
"::-moz-cell-content",":-moz-drag-over",":-moz-first-node","::-moz-focus-inner",
"::-moz-focus-outer",":-moz-focusring",":-moz-full-screen",":-moz-full-screen-ancestor",
":-moz-handler-blocked",":-moz-handler-crashed",":-moz-handler-disabled","::-moz-inline-table",
":-moz-last-node",":-moz-list-bullet",":-moz-list-number",":-moz-loading",
":-moz-locale-dir",":-moz-lwtheme",":-moz-lwtheme-brighttext",":-moz-lwtheme-darktext",
":-moz-math-stretchy",":-moz-math-anonymous",":-moz-only-whitespace","::-moz-page",
"::-moz-page-sequence","::-moz-pagebreak","::-moz-pagecontent",":-moz-placeholder",
":-moz-read-only",":-moz-read-write","::-moz-selection","::-moz-scrolled-canvas",
"::-moz-scrolled-content","::-moz-scrolled-page-sequence",":-moz-suppressed",
":-moz-submit-invalid","::-moz-svg-foreign-content",":-moz-system-metric",
"::-moz-table","::-moz-table-cell","::-moz-table-column","::-moz-table-column-group",
"::-moz-table-outer","::-moz-table-row","::-moz-table-row-group",":-moz-tree-checkbox",
":-moz-tree-cell",":-moz-tree-cell-text",":-moz-tree-column",":-moz-tree-drop-feedback",
":-moz-tree-image",":-moz-tree-indentation",":-moz-tree-line",":-moz-tree-progressmeter",
":-moz-tree-row",":-moz-tree-separator",":-moz-tree-twisty",":-moz-ui-invalidGecko",
":-moz-ui-validGecko",":-moz-user-disabled","::-moz-viewport","::-moz-viewport-scroll",
":-moz-window-inactive","::-moz-xul-anonymous-block"
];

CSS.property = [
"padding","margin","background","font","overflow",
"border","border-radius",
"border-color","border-width","border-style",
"border-top","border-right","border-bottom","border-left",
"outline","-moz-outline-radius","-moz-column-rule",
"-moz-padding-start","-moz-padding-end",
"-moz-margin-start","-moz-margin-end",
"-moz-border-start","-moz-border-end"
];
CSS.hougen = [];
var s = getComputedStyle(document.documentElement, null);
for(var i = 0, p; p = s[i]; i++) {
	p[0] === "-" ? CSS.hougen.push(p) : CSS.property.push(p);
}

CSS.colors = [
'aliceblue','antiquewhite','aqua','aquamarine','azure','beige','bisque','black',
'blanchedalmond','blue','blueviolet','brass','brown','burlywood','cadetblue',
'chartreuse','chocolate','coolcopper','copper','coral','cornflower',
'cornflowerblue','cornsilk','crimson','cyan','darkblue','darkbrown','darkcyan',
'darkgoldenrod','darkgray','darkgreen','darkkhaki','darkmagenta',
'darkolivegreen','darkorange','darkorchid','darkred','darksalmon','darkseagreen',
'darkslateblue','darkslategray','darkturquoise','darkviolet','deeppink',
'deepskyblue','dimgray','dodgerblue','feldsper','firebrick','floralwhite',
'forestgreen','fuchsia','gainsboro','ghostwhite','gold','goldenrod','gray',
'green','greenyellow','honeydew','hotpink','indianred','indigo','ivory','khaki',
'lavender','lavenderblush','lawngreen','lemonchiffon','lightblue','lightcoral',
'lightcyan','lightgoldenrodyellow','yellowgreen',

'ActiveBorder','ActiveCaption','AppWorkspace','Background','ButtonFace',
'ButtonHighlight','ButtonShadow','ButtonText','CaptionText','GrayText',
'Highlight','HighlightText','InactiveBorder','InactiveCaption',
'InactiveCaptionText','InfoBackground','InfoText','Menu','MenuText',
'Scrollbar','ThreeDDarkShadow','ThreeDFace','ThreeDHighlight',
'ThreeDLightShadow','ThreeDShadow','Window','WindowFrame','WindowText',

'-moz-activehyperlinktext','-moz-hyperlinktext','-moz-visitedhyperlinktext',
'-moz-buttondefault','-moz-buttonhoverface','-moz-buttonhovertext','-moz-cellhighlight',
'-moz-cellhighlighttext','-moz-field','-moz-fieldtext','-moz-dialog','-moz-dialogtext',
'-moz-dragtargetzone','-moz-mac-accentdarkestshadow','-moz-mac-accentdarkshadow',
'-moz-mac-accentface','-moz-mac-accentlightesthighlight','-moz-mac-accentlightshadow',
'-moz-mac-accentregularhighlight','-moz-mac-accentregularshadow','-moz-mac-chrome-active',
'-moz-mac-chrome-inactive','-moz-mac-focusring','-moz-mac-menuselect','-moz-mac-menushadow',
'-moz-mac-menutextselect','-moz-menuhover','-moz-menuhovertext','-moz-win-communicationstext',
'-moz-win-mediatext','-moz-nativehyperlinktext'
];

var JS_Words = {};
Object.keys(JS).forEach(function(key){
	JS[key].forEach(function(word){
		JS_Words[word] = JS_Style[key];
	});
});

var CSS_Words = {};
Object.keys(CSS).forEach(function(key){
	CSS[key].forEach(function(word){
		CSS_Words[word] = key === "colors" ? "color: " + word + ";" : CSS_Style[key];
	});
});

//JS.regexp_r   = "\\\/\(\(\?\:\\\\\.\|\\\[\(\?\:\\\\\.\|\[\^\\\]\]\)\*\\\]\|\[\^\\\/\\n\]\)\{0\,100\}\)\\\/\(\[gimy\]\*\)";

XML.MComment_r  = '&lt\\;!--[\\s\\S]+?--&gt\\;';
BASE.URL_r      = ['h?t?tps?://\\w+\\.wikipedia\\.org/wiki/[^\\s<]+'
                  ,'(?:h?t?tps?|ftp)://[\\w\\-]+\\.[\\w.\\-]+(?:[\\w#%=~^_?.;:+*/\\-]|&amp\\;)*'
                  ,'(?:chrome|resource)://[\\w/.#()\\-]+'
                  ,'(?:jar:)?file:///\\w:/[\\w/.#()\\-]+'
                  ,'data:\\w+/[a-zA-Z-]+\\;[\\w-]+?\\,[a-zA-Z0-9/+%\\s\\\\]+={0,2}'
                  ].join('|');
BASE.MComment_r = "/\\*[\\s\\S]*?\\*/";
BASE.SComment_r = "//.*";
BASE.DString_r  = '"(?:[^\\n"\\\\]|\\\\.|\\\\\\n)*"';
BASE.SString_r  = "'(?:[^\\n'\\\\]|\\\\.|\\\\\\n)*'";
BASE.CDATA_r    = "&lt\\;\\!\\[CDATA\\[[\\s\\S\]*?\\]\\]&gt\\;";

BASE.R_URL = new RegExp(BASE.URL_r, "gm");

JS.R_ALL = new RegExp([
	BASE.MComment_r
	,BASE.SComment_r
	,BASE.DString_r
	,BASE.SString_r
	,BASE.CDATA_r
	,'[\\w$]+'
].join('|'), "gm");

CSS.R_ALL = new RegExp([
	BASE.MComment_r
	,BASE.DString_r
	,BASE.SString_r
	,'(?::?:|\\b|@)[a-zA-Z\\-]+\\b'
	,'\\!important\\b'
	,'#[0-9A-Fa-f]{3}[0-9A-Fa-f]{3}?'
].join('|'), "gm");

XML.R_ALL = new RegExp([
	XML.MComment_r
	,BASE.CDATA_r
	,BASE.DString_r
	,BASE.SString_r
].join('|'), "gm");

BASE.R_ALL = new RegExp([
	XML.MComment_r
	,BASE.DString_r
	,BASE.SString_r
	,'[\\w$]+'
].join('|'), "gm");

function parseLink(aText) {
	return aText.replace(BASE.R_URL, function(str){
		var url = str;
		if (url.indexOf("data:image/") === 0)
			return '<img src="'+ url.replace(/\\/g, '') +'" alt="'+ str +'">';

		url = url.replace(/^h?t?tp(s)?/,'http$1');
		return '<a href="'+ url +'" style="'+ BASE_Style.URL +'">'+ str +'</a>';
	});
}

function parse(aText, type) {
	aText = aText.replace(/\&/g, '&amp;').replace(/\</g, '&lt;').replace(/\>/g, '&gt;');
	if (type === "CSS") aText = CSSParser(aText);
	else if (type === "JS") aText = JSParser(aText);
	else if (type === "XML") aText = XMLParser(aText);
	else aText = TXTParser(aText);

	aText = parseLink(aText);
	return aText;
}

function JSParser(aText) {
	return aText.replace(JS.R_ALL, function(str, offset, s) {
		if (str.indexOf("//") === 0) {
			return '<span style="'+ BASE_Style.LineComment +'">' + str + '</span>';
		}
		if (str.indexOf("/*") === 0) {
			return '<span style="'+ BASE_Style.MlutiComment +'">' + str + '</span>';
		}
		if (str[0] === "'") {
			return '<span style="'+ BASE_Style.DoubleQuotation +'">' + str.replace(/\"/g, "&quot;").replace(/\'/g, "&apos;") + '</span>';
		}
		if (str[0] === '"') {
			return '<span style="'+ BASE_Style.SingleQuotation +'">' + str.replace(/\"/g, "&quot;").replace(/\'/g, "&apos;") + '</span>';
		}
		if (str.indexOf("&lt;![CDATA[") === 0) {
			if (/^\s*\@|\!important|url\(/.test(str))
				return CSSParser(str);
			return '<span style="'+ (JS_Style.CDATA || BASE_Style.CDATA) +'">' + str + '</span>';
		}
		if (JS_Words[str]) {
			return '<span style="'+ JS_Words[str] +'">' + str + '</span>';
		}
		return str;
	});
}

function XMLParser(aText) {
	return aText.replace(XML.R_ALL, function(str, offset, s) {
		if (str.indexOf("&lt;!--") === 0) {
			return '<span style="'+ BASE_Style.MlutiComment +'">' + str + '</span>';
		}
		if (str[0] === "'") {
			return '<span style="'+ BASE_Style.DoubleQuotation +'">' + str.replace(/\"/g, "&quot;").replace(/\'/g, "&apos;") + '</span>';
		}
		if (str[0] === '"') {
			return '<span style="'+ BASE_Style.SingleQuotation +'">' + str.replace(/\"/g, "&quot;").replace(/\'/g, "&apos;") + '</span>';
		}
		if (str.indexOf("&lt;![CDATA[") === 0) {
			let res = JSParser(str.replace("&lt;![CDATA[", "").replace("]]&gt;", ""));
			return "&lt;![CDATA[" + res + "]]&gt;";
		}
		return str;
	});
}

function CSSParser(aText) {
	return aText.replace(CSS.R_ALL, function(str, offset, s) {
		if (str.indexOf("/*") === 0) {
			return '<span style="'+ BASE_Style.MlutiComment +'">' + str + '</span>';
		}
		if (str[0] === "'") {
			return '<span style="'+ BASE_Style.DoubleQuotation +'">' + str.replace(/\"/g, "&quot;").replace(/\'/g, "&apos;") + '</span>';
		}
		if (str[0] === '"') {
			return '<span style="'+ BASE_Style.SingleQuotation +'">' + str.replace(/\"/g, "&quot;").replace(/\'/g, "&apos;") + '</span>';
		}
		if (str[0] === '#') {
			return '<span style="color:'+ str +';">' + str + '</span>';
		}
		if (CSS_Words[str]) {
			return '<span style="'+ CSS_Words[str] +'">' + str + '</span>';
		}
		return str;
	});
}

function TXTParser(aText) {
	return aText.replace(BASE.R_ALL, function(str, offset, s) {
		if (str.indexOf("/*") === 0) {
			return '<span style="'+ BASE_Style.MlutiComment +'">' + str + '</span>';
		}
		if (str.indexOf("&lt;!--") === 0) {
			return '<span style="'+ BASE_Style.MlutiComment +'">' + str + '</span>';
		}
		if (str[0] === "'") {
			return '<span style="'+ BASE_Style.DoubleQuotation +'">' + str.replace(/\"/g, "&quot;").replace(/\'/g, "&apos;") + '</span>';
		}
		if (str[0] === '"') {
			return '<span style="'+ BASE_Style.SingleQuotation +'">' + str.replace(/\"/g, "&quot;").replace(/\'/g, "&apos;") + '</span>';
		}
		return str;
	});
}

if (window.JSCSS) {
	window.JSCSS.destroy();
	delete window.JSCSS;
}

var _disabled = true;
window.JSCSS = {
	get disabled () _disabled,
	set disabled (bool) {
		if (_disabled != bool) {
			if (bool) {
				gBrowser.mPanelContainer.removeEventListener("DOMContentLoaded", this, false);
			} else {
				gBrowser.mPanelContainer.addEventListener("DOMContentLoaded", this, false);
			}
		}
		var elem = document.getElementById("JSCSS-menuitem");
		if (elem)
			elem.setAttribute("checked", !bool);
		return _disabled = !!bool;
	},
	init: function() {
		var menuitem = document.createElement("menuitem");
		menuitem.setAttribute("id", "JSCSS-menuitem");
		menuitem.setAttribute("label", "JSCSS Highlight");
		menuitem.setAttribute("type", "checkbox");
		menuitem.setAttribute("checked", "true");
		menuitem.setAttribute("autoCheck", "false");
		menuitem.setAttribute("oncommand", "JSCSS.disabled = !JSCSS.disabled;");
		var ins = document.getElementById("devToolsSeparator");
		ins.parentNode.insertBefore(menuitem, ins);

		var menu = document.createElement("menu");
		menu.setAttribute("id", "JSCSS-context-menu");
		menu.setAttribute("label", "JSCSS Highlight");
		var popup = menu.appendChild(document.createElement("menupopup"));
		popup.setAttribute("id", "JSCSS-context-menupopup");
		var menuitem = popup.appendChild(document.createElement("menuitem"));
		menuitem.setAttribute("label", "JavaScript");
		menuitem.setAttribute("oncommand", "JSCSS.write(gContextMenu.target, 'JS');");
		var menuitem = popup.appendChild(document.createElement("menuitem"));
		menuitem.setAttribute("label", "CSS");
		menuitem.setAttribute("oncommand", "JSCSS.write(gContextMenu.target, 'CSS');");
		var menuitem = popup.appendChild(document.createElement("menuitem"));
		menuitem.setAttribute("label", "XML");
		menuitem.setAttribute("oncommand", "JSCSS.write(gContextMenu.target, 'XML');");
		var menuitem = popup.appendChild(document.createElement("menuitem"));
		menuitem.setAttribute("label", "Text");
		menuitem.setAttribute("oncommand", "JSCSS.write(gContextMenu.target, 'TXT');");
		var context = document.getElementById("contentAreaContextMenu");
		context.appendChild(menu);

		this.disabled = false;
		context.addEventListener("popupshowing", this, false);
		window.addEventListener("unload", this, false);
	},
	uninit: function() {
		document.getElementById("contentAreaContextMenu").removeEventListener("popupshowing", this, false);
		this.disabled = true;
	},
	destroy: function() {
		this.disabled = true;
		["JSCSS-menuitem", "JSCSS-context-menu"].forEach(function(id){
			var elem = document.getElementById(id);
			if (elem) elem.parentNode.removeChild(elem);
		}, this);
		this.uninit();
	},
	handleEvent: function(event) {
		switch(event.type){
			case "popupshowing":
				var elem = document.getElementById("JSCSS-context-menu");
				if (elem)
					elem.hidden = !(gContextMenu.target instanceof HTMLPreElement);
				break;
			case "DOMContentLoaded":
				var doc = event.target;
				if (!/css|javascript|plain/.test(doc.contentType) || 
				    doc.location.protocol === "view-source:"
				) return;
				this.run(doc, 300000);
				break;
			case "unload":
				this.uninit();
				break;
		}
	},
	write: function(pre, type) {
		var doc = pre.ownerDocument;
		if (!type) {
			var { contentType, URL } = doc;
			type = contentType.indexOf('javascript') >= 0 ? 'JS' :
				contentType.indexOf('css') >= 0 ? 'CSS' :
				contentType === 'text/plain' ?
					/\.(?:xul|xml)(?:\.txt)?$/.test(URL) ? 'XML' :
					/\.(?:js|jsm|jsee|ng)(?:\.txt)?$/i.test(URL) ? 'JS' :
					/\.(?:css)$/i.test(URL) ? 'CSS' :
					'TXT' :
				'TXT';
		}
		var html = parse(pre.textContent, type);
		var preRange = doc.createRange();
		preRange.selectNodeContents(pre);
		preRange.deleteContents();
		
		var range = doc.createRange();
		range.selectNodeContents(doc.body);
		var df = range.createContextualFragment(html);
		range.detach();
		preRange.insertNode(df);
		preRange.detach();
	},
	run: function(doc, maxLength) {
		var self = this;
		doc || (doc = content.document);
		var pre = doc.getElementsByTagName('pre')[0];
		if (pre.textContent.length > maxLength) {
			var browser = gBrowser.getBrowserForDocument(doc);
			var notificationBox = gBrowser.getNotificationBox(browser);
			var message = "Der Text ist zu lang. Wollen Sie hervorheben? (Es besteht Absturzgefahr!)"
			var buttons = [{
				label: "Ja",
				accessKey: "J",
				callback: function (aNotification, aButton) {
					 self.write(pre);
				}
			}];
			notificationBox.appendNotification(
				message, "JSCSS",
				"chrome://browser/skin/Info.png",
				notificationBox.PRIORITY_INFO_MEDIUM,
				buttons);
			return;
		}
		self.write(pre);
	},
};
JSCSS.init();

})();
